<?php

namespace MyApp\Models;

/**
 * Class HelpDesk
 * @package MyApp\Models
 */
/**
 * Class HelpDesk
 * @package MyApp\Models
 */
class HelpDesk extends Customer
{
    /**
     * HelpDesk constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->employees = array();
    }

    /**
     * Add new employee to help desk
     * @param string $name
     * @param string $role
     */
    public function addEmployee($name, $role)
    {
        $employee = new Employee($name, $role);
        $employees[] = $employee;
    }

    /**
     *
     */
    public function increaseAllSalary()
    {
        foreach ($this->getEmployees() as $employee) {
            $employee->increaseSalary($employee->salary, 100);
        }
    }
}
