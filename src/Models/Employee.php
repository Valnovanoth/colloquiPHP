<?php
namespace MyApp\Models;

/**
 * Class Employee
 * @package MyApp\Models
 */
class Employee
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $role;
    /**
     * @var int
     */
    public $salary;

    /**
     * Employee constructor.
     * @param $name
     * @param $role
     */
    function __construct($name, $role)
   {
      $this->id = rand();
      $this->name = $name;
      $this->role = $role;
      $this->salary = 1000;
   }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }

   /**
    * This function return:
    * 1 if object has name lower than e
    * -1 if object has name bigger than e
    * 0 e1 and object have same name
    *
    * @param  Employee $e1 [description]
    * @param  Employee $e2 [description]
    * @return int          [description]
    */
   public function compareTo(Employee $e)
   {
        return 0;
   }

    /**
     * @param $salary
     * @param $increase
     */
    public function increaseSalary($salary, $increase)
   {
      $salary = $salary + $increase;
   }
}
