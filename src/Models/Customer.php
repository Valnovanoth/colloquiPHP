<?php
namespace MyApp\Models;

/**
 * Class Customer
 * @package MyApp\Models
 */
class Customer
{
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    protected $address;
    /**
     * @var array
     */
    protected $offices;


    /**
     * @var array list of employee
     */
    protected $employees;

    /**
     * Customer constructor.
     * @param $name
     */
    function __construct($name)
   {
      $this->name = $name;
      $this->offices = array();
      $this->employees = array();
   }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return array
     */
    public function getOffices(): array
    {
        return $this->offices;
    }

    /**
     * @param string $name
     * @param string $address
     * @param string $type
     */
    function addOffice($name, $address, $type)
    {
        $office = new Office();
        $office->addName($name);
        $office->addAddress($address);
        $office->addType($type);

        $this->offices[] = $office;
    }

    /**
     * @return array
     */
    public function getEmployees(): array
    {
        return $this->employees;
    }


    /**
     * Add employee to customer
     *
     * @param string $name
     * @param string $role
     */
    public function addEmployee($name, $role) {
        $e = new Employee($name, $role);
        array_push($this->employees, $e);
    }

    /**
     * Sort the employees by name
     *
     * @todo questo metodo è da completare imlementando un metodo che ordini gli impiegati in ordine alfabetico
     * per nome. non sono utilizzabili le funzioni native di PHP ma deve essere creato un algoritmo di ordinamento
     * Per funzionare
     *
     */
    public function sortEmployeesByName()
    {
        // complete
    }
}
