<?php
/**
 * Created by PhpStorm.
 * User: rommel
 * Date: 15/02/18
 * Time: 11.29
 */

namespace MyAppTest\Models;

use Faker\Factory;
use MyApp\Models\Employee;
use PHPUnit\Framework\TestCase;

class EmployeeTest extends TestCase
{
    protected $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    public function testCompareTo()
    {
        $e1 = new Employee('aaaa', 'aaa');
        $e2 = new Employee('bbbb', 'bbb');
        $e3 = new Employee('aaaa', 'ccc');

        $this->assertEquals(1, $e1->compareTo($e2));
        $this->assertEquals(-1, $e2->compareTo($e1));
        $this->assertEquals(0, $e1->compareTo($e3));
    }
}
