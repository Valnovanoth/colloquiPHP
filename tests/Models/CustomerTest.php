<?php
/**
 * User: Andrea Romanello aka rommel
 */

namespace MyAppTest\Models;
use Faker\Factory;
use MyApp\Models\Customer;
use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{
    protected $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();;
    }

    public function test__construct()
    {
        $name = $this->faker->company;
        $c = new Customer($name);
        $this->assertEquals($c->getName(), $name);
    }

    public function testAddOffice()
    {
        $name = $this->faker->company;
        $c = new Customer($name);
        $c->addOffice($this->faker->name, $this->faker->address, '');
        $this->assertNotEmpty($c->getOffices());
    }

    public function testSortingEmployee()
    {
        $c = new Customer('aaa');
        $c->addEmployee($this->faker->firstName, $this->faker->jobTitle);
        $c->addEmployee($this->faker->firstName, $this->faker->jobTitle);
        $c->addEmployee($this->faker->firstName, $this->faker->jobTitle);
        $c->addEmployee($this->faker->firstName, $this->faker->jobTitle);

        $name = '';
        foreach ($c->getEmployees() as $employee) {
            $this->assertLessThan(0, strcasecmp($name, $employee->getName()));
            $name = $employee->getName();
        }
    }
}
